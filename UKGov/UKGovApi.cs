﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace UKGov
{
    public class UKGovApi
    {
        string Uri;

        public UKGovApi() : this("https://www.gov.uk/api/content/")
        {
        }

        public UKGovApi(String inputUri)
        {
            Uri = inputUri;
        }

        public string GetRequest(string url,string type= "application/json")
        {
            try
            {
                var webRequest = WebRequest.Create(Uri + url) as HttpWebRequest;
                webRequest.Method = WebRequestMethods.Http.Get;
                webRequest.Accept = type;

                var response = webRequest.GetResponse() as HttpWebResponse;

                Stream responseStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(responseStream);

                String content = readStream.ReadToEnd();

                response.Close();
                responseStream.Close();
                readStream.Close();

                JToken parsedContent = JToken.Parse(content);
                string formattedContent = parsedContent.ToString(Newtonsoft.Json.Formatting.Indented);
                return formattedContent;

            }
            catch (Exception e)
            {
                FailComponent(e.ToString());
                return e.ToString();
            }
        }

        public string PostRequest(string url, string json)
        {
            try
            {
                var webRequest = WebRequest.Create(Uri + url) as HttpWebRequest;
                webRequest.Method = WebRequestMethods.Http.Post;
                webRequest.ContentType = "application/json; charset=UTF-8";

                using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                var response = webRequest.GetResponse() as HttpWebResponse;
                using (var streamReader = new StreamReader(response.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    return result;
                }

            }
            catch (WebException e)
            {
                string err = e.Message + "/" + ((HttpWebResponse)e.Response).StatusCode + "/" + ((HttpWebResponse)e.Response).StatusDescription;
                FailComponent(err);
                return err;
            }
            catch (Exception e)
            {
                FailComponent(e.ToString());
                return e.ToString();
            }
        }

        private static string FailComponent(string errorMsg)
        {
            return "{'ErrorMessage': '" + errorMsg + "'}";
        }
    }
}
